package si.uni_lj.fri.pbd.lab3;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

public class TitlesFragment extends ListFragment {
    private boolean mDualPane;
    private int mCurCheckPosition = 0;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i("er","1");
        super.onActivityCreated(savedInstanceState);
        Log.i("er","2");

        setListAdapter(new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.titles)));
        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if(mDualPane){
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            showDetails(mCurCheckPosition);
        }
    }

    void showDetails(int index){
        mCurCheckPosition = index;
        Log.i("er","3");

        if(mDualPane){
            getListView().setItemChecked(index, true);
            DetailsFragment details = DetailsFragment.newInstance(index);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.details,details);
            transaction.addToBackStack(null);
            transaction.commit();

        }else{
            Intent intent = new Intent();
            intent.setClass(getActivity(), DetailsActivity.class);
            intent.putExtra("index", index);
            startActivity(intent);
        }
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.i("er","4");
        showDetails(position);
    }
}


